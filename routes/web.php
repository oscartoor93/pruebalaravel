<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/empresas/', 'EmpresaController@indexEmpresas');
Route::get('/empleados/', 'EmpleadoController@indexEmpleados');
Route::get('/empleados/{id}', 'EmpleadoController@obtenerEmpleado')->where('id', '[0-9]+');
Route::get('/empresas/{id}', 'EmpresaController@obtenerEmpresa')->where('id', '[0-9]+');
Route::get('/empleados/nuevo', 'EmpleadoController@crearEmpleado');
Route::get('/empresas/nueva', 'EmpresaController@crearEmpresa');
Route::get('/empleados/{empleado}/editar', 'EmpleadoController@editarEmpleado');
Route::get('/empresas/{empresa}/editar', 'EmpresaController@editarEmpresa');

Route::post('/empleados/{empleado}/', 'EmpleadoController@editarEmpleado');
Route::post('/empresas/{id}/', 'EmpresaController@editarEmpresa');



Route::post('/updateEmpleado', 'EmpleadoController@updateEmpleado');
Route::post('/updateEmpresa', 'EmpresaController@updateEmpresa');

Route::post('/storeEmpleado', 'EmpleadoController@storeEmpleado');
Route::post('/storeEmpresa', 'EmpresaController@storeEmpresa');
Route::get('/empleados/{id}/deleteEmpleado', 'EmpleadoController@deleteEmpleado');
Route::get('/empresas/{id}/deleteEmpresa', 'EmpresaController@deleteEmpresa');


Auth::routes(['register' => false]);

Route::get('/home', 'HomeController@index')->name('home');
    
    Route :: get ('/home', function () {
        
        return view('welcome2');

  });

