
@extends('layout')
@section('content')

<u class="text-primary">
<h1 class="text-primary"> Editar empleado</h1>
</u>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form method="POST" action="{{url('updateEmpleado')}}">
    {{csrf_field()}}

    <input type="hidden" name="id" id="id" value="{{$empleado->id}}">
    <div>
        <b>
            <label for="nombre">Nombre: </label>
        </b>
    </div>
    <input type="text" name="nombre" id="nombre" value="{{$empleado->nombre}}">
    <br>
    <div>
        <b>
            <label for="apellido">Apellido: </label>
        </b>
    </div>
    <input type="text" name="apellido" id="apellido" value="{{$empleado->apellido}}">
    <br>
    <div>
        <b>
            <label for="correo_electronico">Correo: </label>

        </b>
    </div>
    <input type="text" name="correo_electronico" id="correo_electronico" value="{{$empleado->correo_electronico}}">
    <br>
    <div>
        <b>
            <label for="telefono">Telefono: </label>
        </b>
    </div>
    <input type="text" name="telefono" id="telefono" value="{{$empleado->telefono}}">
    <br>
    <div>
        <b>
            <label for="nombre">Nombre Empresa: </label>
        </b>
    </div>
    <div align="left", class="col-lg-3">
    <select class="form-control m-bot15" name="empresa_id" id="empresa_id">
        @if($empresas->count() > 0)
            @foreach($empresas as $empresa)
                <option value="{{$empresa->id}}" {{$empresa->id == $empleado->empresa_id ? 'selected' : ''}}>{{$empresa->nombre}}</option>
            @endForeach
                 @else
            No hay empresas registradas
        @endif
    </select>
    </div>
    <br>
    <button type="submit" class="btn btn-primary">Actualizar Empleado</button>
    <br>
    <a href="{{ url()->previous() }}">Regresar</a>

</form>
@endsection
