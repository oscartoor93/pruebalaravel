@extends('layout')
@section('content')

<br>
<u class="text-primary">
    <br>
<h1 class="text-primary"> LISTA DE EMPLEADOS </h1>
    <br>
</u>
<table class="table">
    <thead>

    <tr>
        <th scope="col">Nombres</th>
        <th scope="col">Apellidos</th>
        <th scope="col">Correo Electronico</th>
        <th scope="col">Telefono</th>
        <th scope="col">Empresa</th>
        <th scope="col">Opciones</th>
    </tr>
    </thead>
    <tbody>

    @forelse($empleados as $empleado)
        <tr>
            <td >
                {{ $empleado->nombre }}
            </td>
            <td>
                {{$empleado->apellido}}
            </td>
            <td>
                {{$empleado->correo_electronico}}
            </td>
            <td>
                {{$empleado->telefono }}
            </td>
            <td>{{$empleado->empresa['nombre']}}</td>
            <td>
                <a href="{{ url("/empleados/{$empleado->id}") }}">Ver detalles |</a>
                <a href="{{ url("/empleados/{$empleado->id}/editar") }}">Editar |</a>
                <a href="{{ url("/empleados/{$empleado->id}/deleteEmpleado" ) }}"onclick="return confirm('¡Esta seguro que desea borrar este dato!')">Eliminar</a>
            </td>
        </tr>

    @empty
        <li>No hay empleados</li>
    @endforelse
    </tbody>

</table>

{{$empleados->links()}}

<a href="{{ url("/empleados/nuevo") }}">Ingresar nuevo empleado</a>
@endsection