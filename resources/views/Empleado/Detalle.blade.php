@extends('layout')

@section('content')

    <u class="text-primary">
        <h1 class="text-primary"> Detalle del empleado </h1>
    </u>
    <div>
        <b>
        <div>
            <b>
                <label for="nombre">Nombre</label>
            </b>
        </div>
        <input readonly="readonly" ,type="text" name="nombre" id="nombre" value="{{$empleado->nombre}}">
        <br>
        <div>
            <b>
                <label for="apellido">Apellido</label>
            </b>
        </div>
        <input readonly="readonly",type="text" name="apellido" id="apellido" value="{{$empleado->apellido}}">
        <br>
        <div>
            <b>
                <label for="correo_electronico">Correo</label>
            </b>
        </div>
        <input readonly="readonly",type="text" name="correo_electronico" id="correo_electronico" value="{{$empleado->correo_electronico}}">
        <br>
        <div>
            <b>
                <label for="telefono">Telefono</label>
            </b>
        </div>

        <input readonly="readonly",type="text" name="telefono" id="telefono" value="{{$empleado->telefono}}">
        <br>
        <br>
        <a href="{{ url()->previous() }}">Regresar</a>
    @endsection















