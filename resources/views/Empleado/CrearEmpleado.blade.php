@extends('layout')
@section('content')

    <br>
    <u class="text-primary">
    <h1 class="text-primary"> CREAR UN NUEVO EMPLEADO</h1>
    </u>
    @if ($errors->any())
        <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
        </div>
    @endif
        <form method="POST" action="{{url('storeEmpleado')}}">
    {!! csrf_field() !!}
        <div>
            <b>
            <label for="nombre">Nombre: </label>
            </b>
        </div>
        <input type="text" name="nombre"/>
        <br>
        <div>
            <b>
            <label for="nombre">Apellido: </label>
            </b>
        </div>
        <input type="text" name="apellido"/>
        <br>
        <div>
            <b>
            <label for="nombre">Correo: </label>
            </b>
        </div>
        <input type="email" name="correo_electronico"/>
        <br>
        <div>
            <b>
            <label for="nombre">Telefono: </label>
            </b>
        </div>
        <input type="telefono" name="telefono"/>
        <br>
    <b>
    <label for="nombre">Empresa: </label>
    </b>
    <div align="leftf", class="col-md-3">
        <select class="form-control m-bot15" name="empresa_id">
        @if($empresas->count() > 0)
            @foreach($empresas as $empresa)
                <option value="{{$empresa->id}}">{{$empresa->nombre}}</option>
            @endForeach
        @else
            No hay empresas registradas
        @endif
    </select>
    </div>

    <br>
    <button type="submit" class="btn btn-primary">Crear Empleado</button>
            <br>
            <a href="{{ url()->previous() }}">Regresar</a>


        </form>

@endsection