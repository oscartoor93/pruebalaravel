@extends('layout')
@section('content')

<u class="text-primary">
<h1 class="text-primary"> Detalles de la empresa </h1>
</u>
<div>
    <b>
        <label for="nombre">Nombre: </label>

    </b>
</div>
<input readonly="readonly" type="text" name="nombre" id="nombre" value="{{$empresa->nombre}}">
<br>
<div>
    <b>
        <label for="apellido">Logotipo: </label>

    </b>
</div>
<input readonly="readonly" type="text" name="logotipo" id="logotipo" value="{{$empresa->logotipo}}">
<br>
<div>
    <b>
        <label for="correo_electronico">Correo: </label>
    </b>
</div>
<input readonly="readonly" type="text" name="correo_electronico" id="correo_electronico" value="{{$empresa->correo_electronico}}">
<br>
<div>
    <b>
        <label for="telefono">Web: </label>
    </b>
</div>
<input  readonly="readonly"type="text" name="url_web" id="url_web" value="{{$empresa->url_web}}">
<br>
<br>
<u class="text-primary">
<h3 class="text-primary">Empleados registrados en la empresa</h3>
</u>
<table class="table">
    <thead>
    <tr>
        <th scope="col">Nombres</th>
        <th scope="col">Apellidos</th>
        <th scope="col"></th>
        <th scope="col"></th>
        <th scope="col"></th>
        <th scope="col"></th>

    </tr>
    </thead>
    <tbody>
    @forelse($empresa->empleados as $item)
        <tr>
            <td >
                {{ $item->nombre }}
            </td>

            <td >
                {{ $item->apellido}}
            </td>
        </tr>

    @empty

        <p>No hay empleados</p>
    @endforelse

    </tbody>
</table>

<a href="{{ url()->previous() }}">Regresar</a>
@endsection
