@extends('layout')
@section('content')
<u class="text-primary">
    <br>
<h1 class="text-primary"> LISTA DE EMPRESAS</h1>
    <br>
</u>
<table class="table">
    <thead>
    <tr>
        <th scope="col">Nombre</th>
        <th scope="col">Logotipo</th>
        <th scope="col">Corrreo Electronico</th>
        <th scope="col">Web</th>
        <th scope="col">Opciones</th>
    </tr>
    </thead>
    <tbody>
    @forelse($empresas as $empresa)
        <tr>
            <td scope="row">
                {{ $empresa->nombre }}
            </td>
            <td>
                {{$empresa->logotipo}}
            </td>
            <td>
                {{$empresa->correo_electronico}}
            </td>
            <td>
                {{$empresa->url_web }}
            </td>
            <td>
                <a href="{{ url("/empresas/{$empresa->id}") }}">Ver detalles |</a>
                <a href="{{ url("/empresas/{$empresa->id}/editar") }}">Editar |</a>
                <a href="{{ url("/empresas/{$empresa->id}/deleteEmpresa" ) }}"onclick="return confirm('¡Esta seguro que desea borrar este dato!')">Eliminar</a>


            </td>
        </tr>
    @empty
        <li>No hay empleados</li>
    @endforelse

    </tbody>
</table>
{{ $empresas->links() }}
<a href="{{ url('empresas/nueva')}}">Crear una nueva empresa</a>
<br>

@include('footer')
    @endsection