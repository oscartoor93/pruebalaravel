@extends('layout')
@section('content')
<u class="text-primary">
<h1 class="text-primary"> Crear una nueva empresa </h1>
</u>

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form method="POST" action="{{url('storeEmpresa')}}">
    {!! csrf_field() !!}

    <div>
        <b>
            <label for="nombre">Nombre: </label>
        </b>
    </div>
    <input type="text" name="nombre"/>
    <br>
    <div>
        <b>
            <label for="nombre">Logotipo: </label>
        </b>
    </div>
    <input type="text" name="logotipo"/>
    <br>
    <div>
        <b>
            <label for="nombre">Correo Electronico: </label>
        </b>
    </div>
    <input type="text" name="correo_electronico"/>
    <br>
    <div>
        <b>
            <label for="nombre">Web: </label>
        </b>
    </div>
    <input type="text" name="url_web"/>
    <br>
    <br>
    <button type="submit" class="btn btn-primary">Crear Empresa</button>
    <br>
    <a href="{{ url()->previous() }}">Regresar</a>



</form>
@endsection