@extends('layout')
@section('content')
<u class="text-primary">
<h1 class="text-primary">Editar empresa</h1>
</u>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form method="POST" action="{{url('updateEmpresa')}}">
    {{csrf_field()}}

    <input type="hidden" name="id" id="id" value="{{$empresa->id}}">
    <div>
        <b>
            <label for="nombre">Nombre: </label>
        </b>
    </div>
    <input type="text" name="nombre" id="nombre" value="{{$empresa->nombre}}">
    <br>
    <div>
        <b>
            <label for="logotipo">Logotipo: </label>
        </b>
    </div>
    <input type="text" name="logotipo" id="logotipo" value="{{$empresa->logotipo}}">
    <br>
    <div>
        <b>
            <label for="correo_electronico">Correo: </label>
        </b>
    </div>
    <input type="text" name="correo_electronico" id="correo_electronico" value="{{$empresa->correo_electronico}}">
    <br>
    <div>
        <b>
            <label for="url_web">url_web: </label>
        </b>
    </div>
    <input type="text" name="url_web" id="url_web" value="{{$empresa->url_web}}">
    <br>
    <br>
    <button type="submit" class="btn btn-primary">Actualizar Empresa</button>
    <br>
    <a href="{{ url()->previous() }}">Regresar</a>


</form>
@endsection