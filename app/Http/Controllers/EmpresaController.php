<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Empresa;

class EmpresaController extends Controller
{


    public function indexEmpresas(){

        $listEmpresas= Empresa::paginate(10);
        return view('Empresa/IndexEmpresa',['empresas'=>$listEmpresas]);
    }

    public function obtenerEmpresa($id){

        //$empresa = Empresa::Find($id);
        $empresa = Empresa::with('empleados')->where('id',$id)->first();
        return view('Empresa/Detalle',['empresa'=>$empresa]);


    }

    public function crearEmpresa(){


        return view('Empresa/CrearEmpresa');
    }

    public function storeEmpresa(Request $request){


        $datos = $request->all();
        $nuevaEmpresa = new Empresa;
        $nuevaEmpresa->nombre = $datos['nombre'];
        $nuevaEmpresa->logotipo= $datos['logotipo'];
        $nuevaEmpresa->correo_electronico = $datos['correo_electronico'];
        $nuevaEmpresa->url_web = $datos['url_web'];

        $nuevaEmpresa->save();
        return redirect('empresas');
    }

    public function editarEmpresa($id){

        $empresa = Empresa::find($id);

        return view('Empresa/EditarEmpresa',['empresa'=>$empresa]);

    }

    public function updateEmpresa(Request $request){

        $data = $request->validate([
            'id' => '',
            'correo_electronico' => '',
            'url_web' => 'required',
            'nombre' => 'required|max:20',
            'logotipo' => 'required',
        ]);

        $empresa = Empresa::find($data['id']);
        $empresa->nombre = $data['nombre'];
        $empresa->logotipo = $data['logotipo'];
        $empresa->correo_electronico = $data['correo_electronico'];
        $empresa->url_web = $data['url_web'];

        $empresa->save();
        return redirect("empresas/");
    }

    public function deleteEmpresa($id){
        $empresa = Empresa::find($id);
        //return $empleado;
        if ($empresa != null){

            $empresa->delete();
        }

        return redirect('empresas');

    }





}
