<?php

namespace App\Http\Controllers;

use App\Empresa;
use Illuminate\Http\Request;
use App\Empleado;

class EmpleadoController extends Controller
{
    public function indexEmpleados()
    {
        $listEmpleados = Empleado::with('empresa')->Paginate(10);


        return view('Empleado/IndexEmpleados',['empleados'=>$listEmpleados]);
    }

    public function obtenerEmpleado($id)
    {
        $empleado = Empleado::Find($id);
        return view('Empleado/Detalle',['empleado'=>$empleado]);
    }

    public function crearEmpleado()
    {
        $listEmpresas = Empresa::all();

        return view('Empleado/CrearEmpleado', ['empresas'=>$listEmpresas]);
    }

    public function storeEmpleado(Request $request){

        $this->validate($request, [
            'nombre' => 'required|max:20',
            'apellido' => 'required',
            'correo_electronico' => 'required|unique:empleados',
            'empresa_id' => 'required'

        ]);
        $datos = $request->all();
        $nuevoEmpleado = new Empleado;
        $nuevoEmpleado->nombre = $datos['nombre'];
        $nuevoEmpleado->apellido = $datos['apellido'];
        $nuevoEmpleado->correo_electronico = $datos['correo_electronico'];
        $nuevoEmpleado->telefono = $datos['telefono'];
        $nuevoEmpleado->empresa_id = $datos['empresa_id'];

        $nuevoEmpleado->save();

        return redirect('empleados');
    }

    public function editarEmpleado($id){

        $empleado = Empleado::find($id);

        $listEmpresas = Empresa::all();

        return view('Empleado/EditarEmpleado',['empleado'=>$empleado, 'empresas'=>$listEmpresas]);

    }

    public function updateEmpleado(Request $request){

        $data = $request->validate([
            'id' =>'',
            'correo_electronico' =>'',
            'telefono' =>'',
            'empresa_id' =>'',
            'nombre' => 'required|max:20',
            'apellido' => 'required',
        ]);
        //$data = $request->all();

        $empleado = Empleado::find($data['id']);
        $empleado->nombre = $data['nombre'];
        $empleado->apellido = $data['apellido'];
        $empleado->correo_electronico = $data['correo_electronico'];
        $empleado->telefono = $data['telefono'];
        $empleado->empresa_id = $data['empresa_id'];


        $empleado->save();
        return redirect("empleados/");
    }

    public function deleteEmpleado($id){
        $empleado = Empleado::find($id);
        //return $empleado;
        if ($empleado != null){

            $empleado->delete();
        }

        return redirect('empleados/');

    }
}
