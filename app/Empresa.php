<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    protected $table = 'empresas';

    public function empleados()
    {
        return $this->hasMany('App\Empleado', 'empresa_id');
    }
}

