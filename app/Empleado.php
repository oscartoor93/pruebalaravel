<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empleado extends Model
{
    protected $table = 'empleados';
    protected $fillable = ['empresa_id', 'nombre', 'apellido', 'correo_electronico', 'telefono'];

    public function empresa()
    {
        return $this->belongsTo('App\Empresa', 'empresa_id');
    }
}
