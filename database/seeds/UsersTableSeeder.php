<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'superadmin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('prueba2019'),
        ]);
        DB::table('empresas')->insert([
            'nombre' => 'empresa 1',
            'correo_electronico'=>'empresa@hotmail.com',
            'logotipo'=>'',
            'url_web' => 'www.sdd.cs'
        ]);
        DB::table('empleados')->insert([
            'nombre' => 'oscar',
            'apellido' => 'martinez',
            'correo_electronico'=>'coreeo@hotmail.com',
            'telefono'=>'12312144',
            'empresa_id' => 1
        ]);

    }

}