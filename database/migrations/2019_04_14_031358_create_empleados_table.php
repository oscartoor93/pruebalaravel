<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpleadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empleados', function (Blueprint $table) {
            $table->increments('id');
            $table->Integer('empresa_id')->unsigned()->nullable();
            $table->string('nombre')->nullable($value=false);
            $table->string('apellido');
            $table->string('correo_electronico',100)->unique();
            $table->string('telefono')->nullable();;
            $table->timestamps();

            $table->foreign('empresa_id')
                ->references('id')->on('empresas')
                ->onDelete('cascade');



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        if (Schema::hasTable('empleados')) {

            Schema::table('empleados', function (Blueprint $table) {

                $table->dropForeign(['empresa_id']);

            });
        }


        Schema::dropIfExists('empleados');
    }
}
